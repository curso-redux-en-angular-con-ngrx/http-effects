import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { mergeMap, tap, map, catchError } from 'rxjs/operators';
import * as usuariosActions from '../actions/usuarios.actions';
import { UsuarioService } from '../../services/usuario.service';
import { of } from "rxjs";

@Injectable()
export class UsuariosEffects {
    
    constructor(
        private actions$: Actions,
        private usuarioService: UsuarioService
    ){}

    cargarUsuarios$ = createEffect(
        // callback que regresa un observable
        () => this.actions$.pipe(
            ofType(usuariosActions.cargarUsuarios),
            //tap( data => console.log('effect tap', data)),
            // nuevo obs mesclado con el anterior
            mergeMap(
                () => this.usuarioService.getUsers()
                .pipe(
                    //tap( data => console.log('getUsers effect', data))
                    map( users => usuariosActions.cargarUsuariosSuccess({usuarios: users})),
                    catchError(err => of (usuariosActions.cargarUsuariosError({payload: err})))
                )
            )
        )
    );
}