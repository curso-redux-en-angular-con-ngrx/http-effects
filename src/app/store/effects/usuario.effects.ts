import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { mergeMap, tap, map, catchError } from 'rxjs/operators';
import * as usuarioActions from '../actions/usuario.actions';
import { UsuarioService } from '../../services/usuario.service';
import { of } from "rxjs";

@Injectable()
export class UsuarioEffects {
    
    constructor(
        private actions$: Actions,
        private usuarioService: UsuarioService
    ){}

    cargarUsuario$ = createEffect(
        // callback que regresa un observable
        () => this.actions$.pipe(
            ofType(usuarioActions.cargarUsuario),
            //tap( data => console.log('effect tap', data)),
            // nuevo obs mesclado con el anterior
            mergeMap(
                (action) => this.usuarioService.getUserById(action.id)
                .pipe(
                    //tap( data => console.log('getUsers effect', data))
                    map( user => usuarioActions.cargarUsuarioSuccess({usuario: user})),
                    catchError(err => of (usuarioActions.cargarUsuarioError({payload: err})))
                )
            )
        )
    );
}